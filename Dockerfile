#
## @brief Dockerfile used to build a Debian image including
## a pseudo development environment (sysadmin like)
## @author Joel LE CORRE <git@sublimigeek.fr>
#

FROM docker/compose:debian-1.26.0-rc3

# Timezone configuration
ENV TZ Europe/Paris

# Force non interactive installation of packages
ENV DEBIAN_FRONTEND noninteractive

# ~~~~~~~~~~~~~~~~~~~~~
# Environment variables
# ~~~~~~~~~~~~~~~~~~~~~

ENV USERNAME "xduthil"
# https://golang.org/dl/
ENV GOLANG_VERSION "1.14"
# https://github.com/ansible/ansible/releases
ENV ANSIBLE_VERSION "2.7.12"
# https://github.com/ansible/ansible-lint/releases
ENV ANSIBLE_LINT_VERSION "4.1.0"
# https://docs.openstack.org/newton/user-guide/common/cli-install-openstack-command-line-clients.html
ENV OPENSTACK_CLIENT_VERSION "3.19.0"
# https://kubernetes.io/docs/tasks/tools/install-kubectl/
ENV KUBECTL_VERSION "1.15.1-00"
# https://learn.hashicorp.com/terraform/getting-started/install.html
ENV TERRAFORM_VERSION "0.11.14"
ENV TERRAFORM_ARCH "linux_amd64"

# Updating system
RUN \
  apt update && \
  apt -y dist-upgrade

# Installation of useful tools
RUN \
  apt-get install -y apt-utils git screen python3 pass procps \
  vim wget curl telnet jq shellcheck ca-certificates gettext \
  gnupg make openssl openssh-client python3-pytest python-pip sudo \
  yamllint bash-completion htop net-tools dnsutils apt-transport-https unzip jid

# # Ansible installation
# RUN \
#  wget --quiet https://github.com/ansible/ansible/archive/v${ANSIBLE_VERSION}.tar.gz -O - | tar xz -C /opt && \
#  mv /opt/ansible-${ANSIBLE_VERSION} /opt/ansible && \
#  cd /opt/ansible && \
#  pip install -r ./requirements.txt && \
#  python setup.py install

# Ansible linter installation
RUN \
  pip install ansible-lint==${ANSIBLE_LINT_VERSION}

# Add the docker group
RUN \
  groupadd -g 999 docker

# Link host's user in container
RUN \
  useradd -d /home/${USERNAME} ${USERNAME} && \
  usermod -aG docker ${USERNAME}

# Add user in sudoers group
RUN \
  echo "${USERNAME} ALL=(ALL:ALL) NOPASSWD: ALL" > /etc/sudoers.d/${USERNAME}

# OpenStack CLI installation
RUN \
  pip install python-openstackclient==${OPENSTACK_CLIENT_VERSION}

# Kubectl CLI installation
# RUN \
#   curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add - && \
#   echo "deb http://apt.kubernetes.io/ kubernetes-xenial main" | tee -a /etc/apt/sources.list.d/kubernetes.list && \
#   apt-get update && \
#   apt-get install -y kubectl=${KUBECTL_VERSION}
RUN \
  sudo apt-get install -y apt-transport-https && \
  curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add - && \
  echo "deb https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee -a /etc/apt/sources.list.d/kubernetes.list && \
  sudo apt-get update && \
  sudo apt-get install -y kubectl

# Terraform installation
RUN \
  mkdir -p /opt/terraform && \
  wget -q https://releases.hashicorp.com/terraform/${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_${TERRAFORM_ARCH}.zip -O /opt/terraform/terraform.zip && \
  unzip -q /opt/terraform/terraform.zip -d /opt/terraform/  && \
  chmod +x /opt/terraform/terraform

# Golang installation
RUN \
  wget --quiet https://dl.google.com/go/go${GOLANG_VERSION}.linux-amd64.tar.gz -O - | tar xz -C /opt/

# Environment variables used by Golang
# https://golang.org/dl/
ENV GOPATH "/home/$USERNAME/go"
ENV GOEXEC "/opt/go/bin/go"

# CFSSL installation
RUN \
  ${GOEXEC} get -u github.com/cloudflare/cfssl/cmd/cfssl && \
  ${GOEXEC} get -u github.com/cloudflare/cfssl/cmd/cfssljson

# Helm installation
RUN \
  curl https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3 | bash 

# Zsh installation
RUN \
  apt install -y zsh && \
  sh -c "$(wget -O- https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"

  
# Image cleanup
RUN \
  apt-get -y autoremove && \
  apt-get -y autoclean && \
  apt-get -y clean && \
  rm -rf /var/lib/apt/lists/*

# Switch container with the good user workspace
USER ${USERNAME}

# Force workspace to use
WORKDIR /home/${USERNAME}

# Keep containuer up and running
CMD ["sleep", "infinity"]
