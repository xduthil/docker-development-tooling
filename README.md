# Develoment environment based on Debian Docker container

## Short description
If you are really limited in your working environment but you have acces to a Docker daemon, It can be useful to {work|develop}
directly in a containerized environement. To do this, you can use this git repository and customize it to fit your needs.  

This Docker image is based on Debian Linux distribution that's composed entirely of free and open-source software.  
I have only added some useful tools/packages and configurations to simplify the use of this image.  

Non exhaustive list of installed tools  :  

Softwares     | Release number
--------------|----------------------
git           | latest
ansible       | 2.7.12
python3       | latest
pass          | latest
shellcheck    | latest available
yamllint      | latest available
ssh-ident     | latest on master
golang        | 1.12.7
kubectl       | 1.15.0
openstack-cli | 3.19.0
terraform     | 0.11.14
venom         | 0.26.0

See `Dockerfile` for more informations about the customizations.  

## How to build image
Before to build this image, you will need to use the following git repository : [docker-debian-base](https://gitlab.com/jlecorre/docker-debian-base)  
After this, you can get the code and build your own image with this few command lines : 
```shell
git clone ${repository}
cd ${repository}
# build docker image
docker build --rm -t ${docker-image-name}:${image-tag} .
# run docker container
docker container run -d -ti --name ${container-name} -v $HOME:$HOME \
-v $(dirname $SSH_AUTH_SOCK):$(dirname $SSH_AUTH_SOCK) -e SSH_AUTH_SOCK=$SSH_AUTH_SOCK ${docker-image-name}:${image-tag}
# connect to the container
docker container exec -ti ${container-name} bash
```

Have fun and enjoy ;)
